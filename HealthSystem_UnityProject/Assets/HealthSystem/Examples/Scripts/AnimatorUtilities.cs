﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HealthSystem
{
    public class AnimatorUtilities : MonoBehaviour
    {
        public Animator animator;

        public void PlayFromStart(string animation)
        {
            animator.Play(animation, 0, 0f);
        }
    }
}