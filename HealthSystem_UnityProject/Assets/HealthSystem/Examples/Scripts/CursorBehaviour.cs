﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HealthSystem
{
    public class CursorBehaviour : MonoBehaviour
    {
        public UnityEvent OnLeftClick;
        public UnityEvent OnRightClick;

        // Update is called once per frame
        void Update()
        {
            UpdateControls();
        }

        private void UpdateControls()
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            position.z = transform.position.z;

            transform.position = position;

            if (Input.GetMouseButtonDown(0))
                OnLeftClick.Invoke();
            if (Input.GetMouseButtonDown(1))
                OnRightClick.Invoke();
        }
    }
}
