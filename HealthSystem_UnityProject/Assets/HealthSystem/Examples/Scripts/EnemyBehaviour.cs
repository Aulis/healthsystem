﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HealthSystem
{
    public class EnemyBehaviour : MonoBehaviour
    {
        public GameObject deathEffectPrefab;

        public void OnDeath()
        {
            GameObject deathEffect = GameObject.Instantiate<GameObject>(deathEffectPrefab);
            deathEffect.transform.position = transform.position;
            GameObject.Destroy(gameObject);
        }
    }
}
