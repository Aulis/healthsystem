﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HealthSystem
{
    public class Collider2DDetector : MonoBehaviour
    {
        public Transform referenceTransform;

        public float detectorRadius;
        public LayerMask solidLayers;

        public UnityEventGameObject OnDetected;

        public bool debug = false;
        public Color debugColor = Color.white;

        public void CheckForCollider()
        {
            Collider2D collider = Physics2D.OverlapCircle(referenceTransform.position, detectorRadius, solidLayers);
            if (collider != null)
                OnDetected.Invoke(collider.gameObject);
        }

        private void OnDrawGizmos()
        {
            if (debug)
            {
                Gizmos.color = debugColor;
                Gizmos.DrawWireSphere(referenceTransform.position, detectorRadius);
            }
        }
    }
}
