﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HealthSystem
{
    public class WeaponManagement : MonoBehaviour
    {
        public DamageApplier damageApplier;

        public Weapon[] weapons;

        private int currentWeaponIndex = 0;

        // Start is called before the first frame update
        void Start()
        {
            ChangeWeapon(0);
        }

        public void ChangeWeapon(int weaponIndex)
        {
            if (weaponIndex < 0 || weaponIndex >= weapons.Length)
                return;

            for (int i = 0; i < weapons.Length; i++)
            {
                if (i == weaponIndex)
                {
                    weapons[i].spriteObject.SetActive(true);
                    damageApplier.attackData = weapons[i].attackData;
                }
                else
                    weapons[i].spriteObject.SetActive(false);
            }

            currentWeaponIndex = weaponIndex;
        }

        public void CycleWeapons()
        {
            currentWeaponIndex++;
            if (currentWeaponIndex >= weapons.Length)
                currentWeaponIndex = 0;

            ChangeWeapon(currentWeaponIndex);
        }

        public void Attack(GameObject go)
        {
            damageApplier.AttemptApplyDamage(go);
        }

        [System.Serializable]
        public class Weapon
        {
            public GameObject spriteObject;
            public AttackData attackData;
        }
    }
}
