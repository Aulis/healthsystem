﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Describes attacks. As a scriptable type object these attacks can be created and configured in the editor.
//The damage type array contains references to EnumScriptable objects, which are also created and configured in the editor.
//Damage types are compared against HealthBehaviour's vulnerabilities.
//An attack can have multiple damage types.

namespace HealthSystem
{
    [CreateAssetMenu]
    public class AttackData : ScriptableObject
    {
        public int damage;
        public EnumScriptable[] damageTypes;
    }
}
