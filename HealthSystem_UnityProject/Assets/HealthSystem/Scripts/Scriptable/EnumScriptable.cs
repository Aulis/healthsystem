﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A scriptable class for simple object comparisons

namespace HealthSystem
{
    [CreateAssetMenu]
    public class EnumScriptable : ScriptableObject
    {
        public string enumName = "";

#if UNITY_EDITOR
        [TextArea]
        public string description = "";
#endif
    }
}
