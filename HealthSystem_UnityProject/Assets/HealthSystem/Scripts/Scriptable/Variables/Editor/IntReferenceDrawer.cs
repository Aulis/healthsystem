﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//A property drawer for the IntReference class. Creates a single property field and a popup menu with options for using either intValue or scriptableValue.

namespace HealthSystem
{
    [CustomPropertyDrawer(typeof(IntReference))]
    public class IntReferenceDrawer : PropertyDrawer
    {
        private readonly string[] popupOptions =
            {"Use Value", "Use Scriptable"};

        private GUIStyle popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (popupStyle == null)
            {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            //Get properties
            SerializedProperty useValue = property.FindPropertyRelative("useValue");
            SerializedProperty intValue = property.FindPropertyRelative("intValue");
            SerializedProperty scriptableValue = property.FindPropertyRelative("scriptableValue");

            //Calculate rect for menu button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;

            position.xMin = buttonRect.xMax;

            //Store old indent level and set it to 0, the prefix takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            //Create the popup button and options
            int result = EditorGUI.Popup(buttonRect, useValue.boolValue ? 0 : 1, popupOptions, popupStyle);

            useValue.boolValue = result <= 0 ? true : false;

            //Create the field for either intValue or scriptableValue
            EditorGUI.PropertyField(position, useValue.boolValue ? intValue : scriptableValue, GUIContent.none);

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}