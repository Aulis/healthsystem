﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//IntReference is a class that implements an int variable with the option of using either a value type, or a scriptable IntVariable reference.

namespace HealthSystem
{
    [Serializable]
    public class IntReference
    {
        public bool useValue = true;
        public int intValue;
        public IntVariable scriptableValue;

        public int Value
        {
            set
            {
                if (useValue)
                    intValue = value;
                else
                    scriptableValue.SetValue(value);
            }
            get { return useValue ? intValue : scriptableValue != null ? scriptableValue.value : -1; }
        }

        public IntReference()
        {
            useValue = true;
        }

        public IntReference(int value)
        {
            useValue = true;
            intValue = value;
        }

        public static implicit operator float(IntReference reference)
        {
            return reference.Value;
        }
    }
}