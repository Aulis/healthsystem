﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A scriptable object implementation of an int variable.
//Can be used to easily share a variable between multiple scripts without them having to reference eachother.
//-For example the player's current health between HealthBehaviour and UI

namespace HealthSystem
{
    [CreateAssetMenu]
    public class IntVariable : ScriptableObject
    {
        public int value;

#if UNITY_EDITOR
        [TextArea]
        public string Description = "";
#endif

        public void SetValue(int newValue)
        {
            value = newValue;
        }

        public void SetValue(IntVariable newValue)
        {
            value = newValue.value;
        }

        public void AddValue(int newValue)
        {
            value += newValue;
        }

        public void AddValue(IntVariable newValue)
        {
            value += newValue.value;
        }
    }
}