﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

//A property drawer for the TextReference class. Creates a single property field and a popup menu with options for using either uiText, textMesh or tmProText variables.
//Requires installing TextMeshPro.

namespace HealthSystem
{
    [CustomPropertyDrawer(typeof(TextReference))]
    public class TextReferenceDrawer : PropertyDrawer
    {
        private readonly string[] popupOptions =
            {"Use UIText", "Use TextMesh", "Use TextMeshPro"};

        private GUIStyle popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (popupStyle == null)
            {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            //Get properties

            SerializedProperty uiText = property.FindPropertyRelative("uiText");
            SerializedProperty textMesh = property.FindPropertyRelative("textMesh");
            SerializedProperty tmProText = property.FindPropertyRelative("tmProText");
            SerializedProperty option = property.FindPropertyRelative("option");

            //Calculate rect for menu button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;

            position.xMin = buttonRect.xMax;

            //Store old indent level and set it to 0, the prefix takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            //Create the popup button and options
            int result = EditorGUI.Popup(buttonRect, option.enumValueIndex, popupOptions, popupStyle);

            option.enumValueIndex = result;
            
            //Create the field for the chosen option
            switch(option.enumValueIndex)
            {
                case 0: EditorGUI.PropertyField(position, uiText, GUIContent.none); break;
                case 1: EditorGUI.PropertyField(position, textMesh, GUIContent.none); break;
                case 2: EditorGUI.PropertyField(position, tmProText, GUIContent.none); break;
            }

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}
