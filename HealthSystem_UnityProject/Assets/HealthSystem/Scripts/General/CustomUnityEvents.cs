﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Custom UnityEvent classes for passing parameters dynamically in the editor.

namespace HealthSystem
{
    [System.Serializable]
    public class UnityEventInt : UnityEvent<int> { }

    [System.Serializable]
    public class UnityEventInt2 : UnityEvent<int, int> { }

    [System.Serializable]
    public class UnityEventGameObject : UnityEvent<GameObject> { }

    [System.Serializable]
    public class UnityEventDamageApplier : UnityEvent<DamageApplier> { }

    [System.Serializable]
    public class UnityEventInt2DamageApplier : UnityEvent<int, int, DamageApplier> { }

    [System.Serializable]
    public class UnityEventHealthBehaviour : UnityEvent<HealthBehaviour> { }
}
