﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//IntUIValueUpdater is an integer implementation of AbstractUIValueUpdater. Used to display a value in the chosen text displaying component type (see base class).
//Also able to display an integer type scriptable variable.

namespace HealthSystem
{
    public class IntUIValueUpdater : AbstractUIValueUpdater<int>
    {
        public IntVariable intVariable;

        protected override string GetText()
        {
            return GetText(intVariable != null ? intVariable.value.ToString() : string.Empty);
        }
    }
}
