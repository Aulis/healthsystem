﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

//Text reference encapsulates various text classes for setting text easily. Text reference has it's own property drawer (TextReferenceDrawer) to minimize editor clutter.
//Requires installing TextMeshPro.

namespace HealthSystem
{
    [Serializable]
    public class TextReference
    {
        public Text uiText;
        public TextMesh textMesh;
        public TextMeshProUGUI tmProText;

        public TextOptions option = TextOptions.UITEXT;

        public enum TextOptions
        {
            UITEXT,
            TEXTMESH,
            TMPROTEXT
        }

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        //Property for setting or getting text with the chosen option
        public string TextValue
        {
            set
            {
                switch(option)
                {
                    case TextOptions.UITEXT: if(uiText != null) uiText.text = value; break;
                    case TextOptions.TEXTMESH: if (textMesh != null) textMesh.text = value; break;
                    case TextOptions.TMPROTEXT: if (tmProText != null) tmProText.text = value; break;
                }
            }

            get
            {
                switch (option)
                {
                    case TextOptions.UITEXT: return uiText != null ? uiText.text : string.Empty;
                    case TextOptions.TEXTMESH: return textMesh != null ? textMesh.text : string.Empty;
                    case TextOptions.TMPROTEXT: return tmProText != null ? tmProText.text : string.Empty;
                    default: return string.Empty;
                }
            }
        }

        //Property for checking if the chosen option reference is null
        public bool IsNull
        {
            get
            {
                switch (option)
                {
                    case TextOptions.UITEXT: return uiText == null;
                    case TextOptions.TEXTMESH: return textMesh == null;
                    case TextOptions.TMPROTEXT: return tmProText == null;
                    default: return true;
                }
            }
        }

        //----------------------------------------------
        //OTHER METHODS
        //----------------------------------------------

        //Constructor
        public TextReference()
        {
            option = TextOptions.UITEXT;
        }
    }
}
