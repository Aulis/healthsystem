﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Abstract base class for UIValueUpdaters. Used to display a value in the chosen text displaying component type.
//Can be inherited to create integer, float or other UIValueUpdaters.

namespace HealthSystem
{
    public abstract class AbstractUIValueUpdater<T> : MonoBehaviour
    {
        public TextReference textReference;
        public string prefix = "";
        public string suffix = "";
        public bool setOnAwake = false;

        private void Awake()
        {
            if (setOnAwake)
                ShowValue();
        }

        protected string GetText(string text)
        {
            return prefix + text + suffix;
        }

        public void OnValueChange(T amount)
        {
            textReference.TextValue = GetText(amount.ToString());
        }

        public void ShowValue()
        {
            textReference.TextValue = GetText();
        }

        //For displaying a scriptable variable
        protected abstract string GetText();
    }
}
