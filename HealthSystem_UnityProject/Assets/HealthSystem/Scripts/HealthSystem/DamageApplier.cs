﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//The DamageApplier script stores attack related values, handles related events and applies damage to HealthBehaviour objects

namespace HealthSystem
{
    public class DamageApplier : MonoBehaviour
    {

        public AttackData attackData;                       //AttackData contains the damage value and damage types.
        public UnityEventHealthBehaviour OnDamageApplied;   //Event parameters: "damage receiver".
        public UnityEventHealthBehaviour OnDamageFailed;    //Event parameters: "damage receiver".

        //Attempt to apply damage to a HealthBehaviour object. Returns true if damage was applied successfully, false if not.
        public bool AttemptApplyDamage(HealthBehaviour health)
        {
            if (health == null)
                return false;

            //If either the DamageApplier or HealthBehaviour have no defined damage types apply the damage.
            if (attackData.damageTypes.Length <= 0 || health.damageTypeVulnerabilities.Length <= 0)
            {

                ApplyDamage(health);
                return true;
            }
            //Else find any damage type match in DamageApplier and HealthBehaviour and apply damage
            else
            {
                for (int i = 0; i < attackData.damageTypes.Length; i++)
                {
                    for (int j = 0; j < health.damageTypeVulnerabilities.Length; j++)
                    {
                        if (attackData.damageTypes[i] == null || health.damageTypeVulnerabilities[j] == null)
                            continue;

                        if (attackData.damageTypes[i] == health.damageTypeVulnerabilities[j])
                        {
                            ApplyDamage(health);
                            return true;
                        }
                    }
                }
            }

            //If no damage type matches were found, the attack failed
            OnDamageFailed.Invoke(health);
            return false;
        }

        //Attempt to apply damage to a GameObject. Returns true if damage was applied successfully, false if not.
        public bool AttemptApplyDamage(GameObject go)
        {
            if (go == null)
                return false;

            HealthBehaviour health = go.GetComponent<HealthBehaviour>();
            return AttemptApplyDamage(health);
        }

        private void ApplyDamage(HealthBehaviour health)
        {
            health.ProcessDamage(attackData.damage, this);
            OnDamageApplied.Invoke(health);
        }
    }
}
