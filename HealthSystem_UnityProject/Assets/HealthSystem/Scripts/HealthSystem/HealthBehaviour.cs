﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//The HealthBehaviour script handles health related values and events.
//About the variables:
//Health and maxHealth are IntReference objects and can be either defined in the inspector window, or with scriptable int variables.
//The user can set vulnerabilities to different damage types with the damageTypeVulnerabilities array. If left empty any type will do damage. Damage types are scriptable objects of type EnumScriptable.
//-It's adviced to create damage types for each type of attack you want to have in your game and use these with your HealthBehaviours and DamageAppliers.
//A set of custom Unity Events provide a generic way to tie up to other scripts in the editor or trigger scriptable GameEvents.
//-The events have various parameters providing information, but they can also be used to trigger parameterless methods.

namespace HealthSystem
{
    public class HealthBehaviour : MonoBehaviour
    {
        public IntReference health;
        public IntReference maxHealth;

        [Tooltip("An array of damage types this HealthBehaviour is vulnerable to. If left empty any type will do damage.")]
        public EnumScriptable[] damageTypeVulnerabilities;

        public bool resetHealthOnEnable = true;

        [Header("Events")]
        public UnityEventInt2DamageApplier OnDamage;    //Event parameters: "current health", "damage" and "damage provider". Use with UI updates, AI etc.
        public UnityEventDamageApplier OnDeath;         //Event parameters: "damage provider".
        public UnityEventInt OnHealthUpdate;            //Event parameters: "current health". Use with UI updates etc.
        public UnityEventInt2 OnHealthUpdateDelta;      //Event parameters: "current health", "health change". Use with UI updates etc.
        public UnityEvent OnRevive;
        public UnityEvent OnHealthFull;

        //----------------------------------------------
        //PROPERTIES
        //----------------------------------------------

        public bool IsDead
        {
            get { return health <= 0; }
        }

        //----------------------------------------------
        //MONOBEHAVIOUR METHODS
        //----------------------------------------------

        private void OnEnable()
        {
            if (resetHealthOnEnable)
                ResetThis();
        }

        public void Start()
        {
            ResetThis();
        }

        //----------------------------------------------
        //OTHER METHODS
        //----------------------------------------------

        public void ResetThis()
        {
            health.Value = maxHealth.Value;

            OnHealthUpdate.Invoke(health.Value);
            OnHealthUpdateDelta.Invoke(health.Value, 0);
        }

        //SetHealth handles health modification and triggers related events
        public void SetHealth(int newHealth)
        {
            int oldHealth = health.Value;
            health.Value = Mathf.Clamp(newHealth, 0, maxHealth.Value);

            if (newHealth > 0)
            {
                if (newHealth < oldHealth)
                    //Damage can also be applied with this method, without a DamageApplier reference
                    OnDamage.Invoke(health.Value, oldHealth - newHealth, null);
                else if (oldHealth <= 0)
                    OnRevive.Invoke();

                if (newHealth == maxHealth.Value)
                    OnHealthFull.Invoke();
            }
            else if (oldHealth > 0)
                OnDeath.Invoke(null);

            OnHealthUpdate.Invoke(health.Value);
            OnHealthUpdateDelta.Invoke(health.Value, newHealth - oldHealth);
        }

        //ChangeHealth could be used with items or with environment inflicted damage
        public void ChangeHealth(int amount)
        {
            SetHealth(Mathf.Clamp(health.Value + amount, 0, maxHealth.Value));
        }

        //Revive is used to reset health and trigger the related events
        public void Revive()
        {
            SetHealth(maxHealth.Value);
        }

        //ProcessDamage is the method called by DamageApplier when dealing damage. Damage needs to be a positive value.
        public void ProcessDamage(int damage, DamageApplier damageApplier)
        {
            if (damage < 0)
                return;
            if (IsDead)
                return;

            health.Value -= damage;

            //Either OnDeath or OnDamage events are triggered based on remaining health
            if (health.Value <= 0)
            {
                health.Value = 0;
                OnDeath.Invoke(damageApplier);
            }
            else
                OnDamage.Invoke(health.Value, damage, damageApplier);

            OnHealthUpdate.Invoke(health.Value);
            OnHealthUpdateDelta.Invoke(health.Value, -damage);
        }
    }
}
