# HealthSystem #

The HealthSystem Unity project provides some basic health related scripts for games. Scriptable objects allow the user to create a set of easily swappable attacks and damage types.
UnityEvents provide information to other components. Requires TextMeshPro to work.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/Example.png)

- - -

# About key scripts #

Here is a brief description of the key monobehaviour scripts and their editor properties. More documentation for non-monobehaviour scripts can be found in code.

## HealthBehaviour ##

This component adds health functionality to your gameobjects. The health variable has two options; local value and a scriptable IntVariable.

The user can define an array of damage types this HealthBehaviour is vulnerable to. UnityEvents provide information about the HealthBehaviour's state.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/HealthBehaviour%20component.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/HealthSystem%20table.png)

## DamageApplier ##

The DamageApplier component stores attack related values, handles related events and applies damage to HealthBehaviour objects.

The attack is based on an AttackData scriptable object which defines the amount and type of damage.

Provide a HealthBehaviour or Gameobject and call ApplyDamage or AttemptApplyDamage method. See Collider2DDetector.cs in the provided examples.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/DamageApplier%20component.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/DamageApplier%20table.png)

## IntUIValueUpdater ##

Used to display a value in the chosen text displaying component type. Also able to display an integer type scriptable variable.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/IntUIValueUpdater%20component.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/IntUIValueUpdater%20table.png)

## AttackData ##

Describes attacks. As a scriptable object these attacks can be created and configured in the editor. An attack can have multiple damage types.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/AttackData%20scriptable.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/AttackData%20table.png)

## EnumScriptable ##

A scriptable object for simple object comparisons. Use these to create damage types.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/EnumScriptable%20scriptable.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/EnumScriptable%20table.png)

## IntVariable ##

A scriptable object implementation of an int variable. Can be used to easily share a variable between multiple scripts without them having to reference eachother.

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/IntVariable%20scriptable.png)

![Alt text](https://bitbucket.org/Aulis/healthsystem/raw/a147e9d6266825fa3050ffd446708ba02711f233/READMEImages/IntVariable%20table.png)

- - -

# Installation #

Project has been developed in Unity 2022.3.34. Functionality in newer versions is likely, but not guaranteed. Requires TextMeshPro to work.

* Use the included Unity package file to import the scripts and examples to your project.
* In Unity choose 'Assets' -> 'Import package' -> 'Custom package'. Browse and select the HealthSystem_2020-xx-xx.unitypackage file.
* To install TextMeshPro to your project go to 'Window' -> 'Package Manager'. Locate TextMeshPro and install.
* The Examples folder includes a scene with three enemies, each with different vulnerabilities.

- - -

# Usage #

* Create your damage types in your project(EnumScriptable scriptable objects).
* Create an AttackData scriptable object in your project. Set the damage and damage type fields.
* Add HealthBehaviour component to your enemy gameobject. Set Health and MaxHealth fields. Set vulnerabilities (EnumScriptable damage types).
* Configure the HealthBehaviour component's OnDeath event to disable the enemy gameobject.
* Add DamageApplier component to your attacker. Choose the AttackData object.
* Create a script for the attacker that finds the enemy's HealthBehaviour component and passed it to DamageApplier's ApplyDamage method. See Collider2DDetector.cs in the provided examples.
* When the attacker attacks the enemy, the enemy gameobject should disable once it's health is empty.

- - -

# License #

MIT License

Copyright (c) 2020 Petteri Saarela

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.